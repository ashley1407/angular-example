var example1 = angular.module('ExampleApplication', []);

example1.controller('ExampleController', function ($scope, $http) {

    $http.get('people.json').success(function(data){
        if (data.people) {
            $scope.listOfPeople = data.people;
        }
    });

    $scope.addPerson = function(person) {
        console.log(person);
        $scope.listOfPeople.push({name: person.name, age: person.age});
    }

});

example1.controller('AnotherController', function ($scope) {

});